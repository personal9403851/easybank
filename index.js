document.addEventListener('DOMContentLoaded', function () {
    const hamburgerButton = document.querySelector('.hamburger-button')
    const hamburgerMenu = document.querySelector('.hamburger-menu')
    const hamburgerClose = document.querySelector('.hamburger-close')
    const mockupImage = document.querySelector('.mockup-image')

    hamburgerMenu.classList.add('hamburger-menu-hidden');
    hamburgerClose.classList.add('hamburger-close-hidden');

    hamburgerButton.addEventListener('click', function () {
        hamburgerMenu.classList.toggle('hamburger-menu-hidden')
        hamburgerButton.classList.add('hamburger-button-hidden')
        hamburgerClose.classList.toggle('hamburger-close-hidden')
        mockupImage.classList.add('mockup-image-hidden')
    })

    hamburgerClose.addEventListener('click', function () {
        hamburgerClose.classList.toggle('hamburger-close-hidden')
        hamburgerButton.classList.toggle('hamburger-button-hidden')
        hamburgerMenu.classList.toggle('hamburger-menu-hidden')
        mockupImage.classList.toggle('mockup-image-hidden')
    })
})